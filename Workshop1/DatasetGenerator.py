import numpy as np

EXPECTED_A = 3.14159
EXPECTED_B = 4.242

def generateDataset():
    entries = np.random.uniform(-10.0, 10.0, [100, 1])
    noises = np.random.uniform(-3.0, 3.0, [100, 1])
    out = EXPECTED_A * entries + EXPECTED_B + noises
    return entries, out
