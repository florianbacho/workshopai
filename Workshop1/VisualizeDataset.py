import numpy as np
import matplotlib.pyplot as plt
from DatasetGenerator import *

if __name__ == "__main__":
    entries, outs = generateDataset()

    print(entries)
    print(outs)

    plt.plot(entries, outs, 'ro')
    plt.show()
